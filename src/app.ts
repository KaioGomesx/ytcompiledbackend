import express from "express";
import cors from "cors";

import loggingMiddleware from "./middlewares/logging";

import routes from "./routes";

import log from "./utils/logging";

const app = express();

app.use(express.json());
app.use(cors());
app.use(loggingMiddleware);
app.use(routes);

function initApp() {
  app.listen(3030, () =>
    log.info("initApp", "Server Started on http://localhost:3000")
  );
}

export default initApp;
