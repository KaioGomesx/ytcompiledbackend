# Scripts

## `npm run start`: Inicia o projeto em modo de desenvolvimento.

## `npm run dev`: Inicia o projeto em modo de desenvolvimento.

## `npm run build`: Builda o projeto para estar pronto para o ambiente de produção.

<br></br>

# Database e variáveis de ambiente

## A URL para fazer a conexão com o banco de dados deve ficar no arquivo `.env`.

## copie o arquivo `.env.example` para `.env` e preencha a variável com a URL do banco de dados

## Exemplo no linux: `cp .env.example .env`

<br></br>

# Considerações no ambiente de desenvolvimento

## Se possivel usem o editor de texto VSCode para trabalhar no projeto e instalem as seguintes extensões: "EditorConfig for VS Code", "Prettier - Code formatter" e "Code Spell Checker"

<br></br>

# Sobre o desenvolvimento

## Qualquer coisa relacionada a código será escrito em inglês, a extensão "Code Spell Checker" é responsável por idêntificar palavras erradas, as palavras erradas serão sublinhadas em azul no VSCode, sempre que possivem corrijam as palavras erradas.
